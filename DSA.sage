import random 
import hashlib

def key_generator(pubk_file, pirk_file):

	p = 0
	q = 0
	y = 0
	x = 0
	g = 0

	while(q < 2**15):
		q = random_prime(2**16)   
		
	while (not is_prime(p)):
		p = (randint(1,2^46)*2*q)+1 
    
	h = randint(2,p-2)
	F=GF(p) 
	g=F(h)^((p-1)/q)
	x=randint(0,q)
	y=(g^x) % p

	pubk_file.write(str(p))
	pubk_file.write('\n')
	pubk_file.write(str(q))
	pubk_file.write('\n')
	pubk_file.write(str(g))
	pubk_file.write('\n')
	pubk_file.write(str(y))
	pubk_file.close()

	pirk_file.write(str(p))
	pirk_file.write('\n')
	pirk_file.write(str(q))
	pirk_file.write('\n')
	pirk_file.write(str(g))
	pirk_file.write('\n')
	pirk_file.write(str(x))
	pirk_file.close()

	return p, q, g, y, x


def sign_a_file(msg_file, p, q, g, y, x):

	M = msg_file.read()
	H = hashlib.sha1()
	H.update(M)
	msg_file.close()

	h = ''.join(str(ord(c)) for c in H.hexdigest())

	k=randint(0,q)

	r=Mod(power_mod(g, k, p),q)

	i=(k^-1)%q

	s=Mod((int(i)*(int(h)+int(r)*int(x))),int(q))

	file = open('signed_text.dsa','w+')
	file.write(str(r))
	file.write('\n')
	file.write(str(s))
	file.write('\n')
	file.write(M)
	file.close()


def verify_a_file(msg_file, k_file):
	
	r = msg_file.readline()
	s = msg_file.readline()
	M = msg_file.read()
	H = hashlib.sha1()
	H.update(M)
	msg_file.close()
	h = ''.join(str(ord(c)) for c in H.hexdigest())
	h=int(str(h))
	r=int(str(r))
	s=int(str(s))

	p = k_file.readline()
	q = k_file.readline()
	g = k_file.readline()
	y = k_file.readline()
	k_file.close()

	q=int(str(q))
	p=int(str(p))
	g=int(str(g))
	y=int(str(y))

	w = (s^-1)%q
	u1 = Mod((h*w),q)
	u2 = Mod((r*w),q)
	v = Mod(Mod((g^int(u1))*(y^int(u2)),p),q)

	if(v==r):
		print ('\n SIGNATURE VERIFIED \n')
    	new_file = open('verified_message.txt', 'w+')
    	new_file.write(M)
    	print ('\n----------------------------------------------------\n')
	if(v!=r):
		print ('\n SIGNATURE IS NOT VERIFIED \n')
		print ('\n----------------------------------------------------\n')

def main():
	while 1: 
		print """
		WELCOME TO THE DSA PROGRAM


	1. Sign a file
	2. Verify a file 
	3. Exit \n 
	"""
		choice = raw_input('Please choose one of the options above.\n')

		if choice == '1':


			pubKey_file = open('public_key.txt', 'w+')
			pirKey_file = open('private_key.txt', 'w+')

			p, q, g, y, x = key_generator(pubKey_file, pirKey_file)
	    	
			path = raw_input('\n Enter the path of the file you want to sign with DSA: \n')

			text_file = open(path, 'r')

			sign_a_file(text_file, p, q, g, y, x)


		if choice == '2':

			path2 = raw_input('\nEnter the path of the signed data you want to verify: \n')

			signed_message = open(path2, 'r')

			path3 = raw_input('\nEnter the path of the file which keys exist\n')

			key_file = open(path3, 'r')

			verify_a_file(signed_message, key_file)


		if choice == '3':
			sys.exit()

main()